# Gruvbox theme for Microsoft Teams

<p align="center">
    <img src="https://gitlab.com/imn1/gruvbox-msteams/-/raw/master/assets/screenshot.png">
</p>

## Installing
1. choose dark theme in your MS Teams profile
2. import the file as a user script to Tamper/Grease Monkey
